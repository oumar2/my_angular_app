import { Injectable } from '@angular/core';
import { Devise } from '../data/devise';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {map} from 'rxjs/operators';
import { HttpClientModule } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DeviseService {
  public deleteDevise(codeDevise:string): Observable<any>{

let url = `./devise-api/private/role_admin/devise/${codeDevise}`;
return this.http.delete(url);
  }

  constructor(private http:HttpClient) { }

  public rechercherDevises() : Observable< Devise[]>  // avec asymchrone sur type de retour
  {
   //return of(this.tabDevise);
   let url = "./devise-api/public/devise";

   return this.http.get<Devise[]>(url);
  }

  // pour version de service temporaire 

private tabDevise:Devise[]=[
      new Devise("USD", "Dollar",1),
      new Devise("EUR", "Euro",0.9),
      new Devise("GBP", "Livre",0.8),
      new Devise("jpy", "Yen",120)  
];



 public convertir (montant:number,
          codeMonnaieSource:string
         ,codeMonnaieCible:string):Observable<number>{
         let url = `./devise-api/public/convert`+
          `?source=${codeMonnaieSource}&target=${codeMonnaieCible}&amount=${montant}`
      
          return this.http.get<object>(url)
        .pipe(
            map((responseObject)=>{return responseObject["result"];})

        );
        
        //le web service Rest retourn un resultat de type 
        // {{"source":"EUR","target":"USD","amount":200,"result":219.78021978021977}}
        //correspondant à responseData dans le code ci-dessus 
        // map (chose )=> {return valeur transformé; }permet
        // d'extrait que la sous partie result de type number
        
        
          /*  let res =1;
            // code temporaire sans http
            let deviseCible =null;
            let deviseSource = null;
            let DeviseService =null;
            for(let d of this.tabDevise){
              if(d.code==codeMonnaieSource)
                   deviseSource= d;
              if(d.code==codeMonnaieCible)
                   deviseCible= d;
            } 
      res = montant * deviseCible.change / deviseSource.change;
      return of(res);*/
         
    
    }


  
}