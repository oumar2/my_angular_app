import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/common/service/product.service';
import { Product } from 'src/app/common/data/product';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-list-prod',
  templateUrl: './list-prod.component.html',
  styleUrls: ['./list-prod.component.scss']
})
export class ListProdComponent implements OnInit {
  
  categorie:string="divers";
  listeProducts: Product[];

  constructor(private productService:ProductService,
              private route:ActivatedRoute) { }

  ngOnInit() {
    //via subscribe , ion enregistre ici a callback qui
    //sera  utlieurement  re- declecher chaque fois que la fin
    //d'url change /lisprod/cd , liqtprod/dvd
    this.route.params.subscribe(
    (params:Params)=>{this.categorie=params['category']
                      this.fetchProducts()}

    );

    // paths

  }

  fetchProducts(){
    this.productService.getProductsByCategoryObservable(this.categorie)
    .subscribe(
      (products)=>{this.listeProducts=products;}
    );

  }
}
