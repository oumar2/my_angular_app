import { Component, OnInit } from '@angular/core';
//import { strictEqual } from 'assert';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  categories:string[]=['divers','DVD','CD','Livres'];
  
  constructor(private router:Router) { }
  onNavigate(selectedCategory:string){
    let link=['/products','listProd',selectedCategory];
    this.router.navigate(link);
  }
  
  ngOnInit() {
  }

}
