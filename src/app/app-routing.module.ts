import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { BasicComponent } from './basic/basic.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { CalcultvaComponent } from './basic/calcultva/calcultva.component';
import { AdminDeviseComponent } from './admin-devise/admin-devise.component';
import { ProductsComponent } from './products/products.component';
import { ListProdComponent } from './products/list-prod/list-prod.component';


const routes: Routes = [
  { path: 'login', component:LoginComponent },
  { path: 'welcome', component:WelcomeComponent },
  { path: '', redirectTo: '/welcome', pathMatch: 'full'},
  { path: 'basic', component:BasicComponent },
  { path: 'adminDevise', component:AdminDeviseComponent },
  { path: 'calcultva', component:CalcultvaComponent },
  { path: 'products', component:ProductsComponent,
    children:[
      { path: 'listProd/:category', component:ListProdComponent },
      { path: '', redirectTo: 'listProd/divers', pathMatch: 'prefix'}
    ] }
];
/* parametrage de ce que s'affiche palce de <router-outerlet> dans compoenet html */

 /*  pour naviger <a[routrt-link]= "['/login] vers login  </a>*/


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
