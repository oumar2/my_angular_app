import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimulationempruntComponent } from './simulationemprunt.component';

describe('SimulationempruntComponent', () => {
  let component: SimulationempruntComponent;
  let fixture: ComponentFixture<SimulationempruntComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimulationempruntComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimulationempruntComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
