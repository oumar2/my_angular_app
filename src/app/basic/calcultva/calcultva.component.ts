import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calcultva',
  templateUrl: './calcultva.component.html',
  styleUrls: ['./calcultva.component.scss']
})
export class CalcultvaComponent implements OnInit {
   public ht:number ;
   public taux:number =20;
   public tva: number;
   public ttc: number;
   
   public calculer(){

    this.tva =this.ht* this.taux/100;
    this.ttc = Number(this.ht) + this.tva;
    
    //this.tabRes.push(`${this.ht} puissance ${this.taux} =${this.tabRes}`)
  }

  constructor() { }

  ngOnInit() {
  }

}
