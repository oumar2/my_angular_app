import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcultvaComponent } from './calcultva.component';

describe('CalcultvaComponent', () => {
  let component: CalcultvaComponent;
  let fixture: ComponentFixture<CalcultvaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcultvaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcultvaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
