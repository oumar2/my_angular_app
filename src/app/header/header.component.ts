import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input()//valeur modifiable par le composant parent
           //  <app-header [titre]
  titre ="my- default-header";

  ngOnInit() {
  }

}
